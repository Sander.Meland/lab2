package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    int maxSize = 20;
    List<FridgeItem> items = new ArrayList<FridgeItem>();
    List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
    List<FridgeItem> removeItems = new ArrayList<FridgeItem>();
    
    
    public int totalSize(){ // små og store kjøleskaper eller fast på 20
        return maxSize;
    }

    

    @Override //annotation koden er implementert, og definert i en annen fil
    
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        int n = items.size();
        return n;

    
    }


    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge()<20){
            items.add(item);
            return true;
        }
        else{
            return false;
        }
        
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (!items.isEmpty()){
            for (FridgeItem fridgeitem: items){
                if (fridgeitem.equals(item)){
                    removeItems.add(item);
                }
            }
        }
        else{
            throw new NoSuchElementException();
        }
        items.removeAll(removeItems);
        
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        for (FridgeItem item: items){
            if (item.hasExpired()){
                expiredItems.add(item);
            }
        } 
        items.removeAll(expiredItems);
        return expiredItems;
        
    }
}
